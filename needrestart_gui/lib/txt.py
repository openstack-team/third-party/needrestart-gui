#! /usr/bin/env python3

import codecs

try:
    codecs.lookup_error('surrogateescape')
    HAS_SURROGATEESCAPE = True
except LookupError:
    HAS_SURROGATEESCAPE = False
import operator

string_types = str
integer_types = int
class_types = type
text_type = str
binary_type = bytes

_COMPOSED_ERROR_HANDLERS = frozenset((None, 'surrogate_or_replace',
                                      'surrogate_or_strict',
                                      'surrogate_then_replace'))


def extract_str(s, from_txt, to_txt, include_fromto=False, be_tolerant=True):
    pos1 = s.find(from_txt)
    if pos1 == -1:
        return ''
    pos2 = s.find(to_txt, pos1+len(from_txt))
    if pos2 == -1 and be_tolerant and not include_fromto:
        return s[pos1+len(from_txt):]
    if pos2 == -1 and be_tolerant and include_fromto:
        return s[pos1:]
    if pos2 == -1 and not be_tolerant:
        return ''
    if not include_fromto:
        return s[pos1+len(from_txt):pos2-len(to_txt)+ 1]
    return s[pos1:pos2+len(to_txt)]


def filter_mltext(_input, ignore):
    """Filter multi-line text, remove lines with matches a simple text ignore pattern (no regex).
    `ignore` has to be a list.
    """
    filtered_input = ''
    for line in _input.splitlines():
        if not any(i_line in line for i_line in ignore):
            filtered_input += line + '\n'
    return filtered_input


def mltext2array(_input, skip_header=False, sort_key=-1):
    _input = _input.strip(' \t\n\r').split('\n')
    lines = []
    if skip_header:
        del _input[0]
    for row in _input:
        lines.append(row.split())
    if sort_key != -1:
        lines = sorted(lines, key=operator.itemgetter(sort_key))
    return lines


def pluralize(noun, value, suffix='s'):
    """Returns a plural suffix if the value is not 1. By default, 's' is used as
    the suffix.
    From https://kite.com/python/docs/django.template.defaultfilters.pluralize
    """
    if ',' in suffix:
        singular, plural = suffix.split(',')
    else:
        singular, plural = '', suffix
    if int(value) == 1:
        return noun + singular
    return noun + plural


def to_bytes(obj, encoding='utf-8', errors=None, nonstring='simplerepr'):
    """Make sure that a string is a byte string
    """
    if isinstance(obj, binary_type):
        return obj

    original_errors = errors
    if errors in _COMPOSED_ERROR_HANDLERS:
        if HAS_SURROGATEESCAPE:
            errors = 'surrogateescape'
        elif errors == 'surrogate_or_strict':
            errors = 'strict'
        else:
            errors = 'replace'

    if isinstance(obj, text_type):
        try:
            return obj.encode(encoding, errors)
        except UnicodeEncodeError:
            if original_errors in (None, 'surrogate_then_replace'):
                return_string = obj.encode('utf-8', 'surrogateescape')
                return_string = return_string.decode('utf-8', 'replace')
                return return_string.encode(encoding, 'replace')
            raise

    if nonstring == 'simplerepr':
        try:
            value = str(obj)
        except UnicodeError:
            try:
                value = repr(obj)
            except UnicodeError:
                return to_bytes('')
    elif nonstring == 'passthru':
        return obj
    elif nonstring == 'empty':
        return to_bytes('')
    elif nonstring == 'strict':
        raise TypeError('obj must be a string type')
    else:
        raise TypeError('Invalid value %s for to_bytes\' nonstring parameter' % nonstring)

    return to_bytes(value, encoding, errors)


def to_text(obj, encoding='utf-8', errors=None, nonstring='simplerepr'):

    if isinstance(obj, text_type):
        return obj

    if errors in _COMPOSED_ERROR_HANDLERS:
        if HAS_SURROGATEESCAPE:
            errors = 'surrogateescape'
        elif errors == 'surrogate_or_strict':
            errors = 'strict'
        else:
            errors = 'replace'

    if isinstance(obj, binary_type):
        return obj.decode(encoding, errors)

    if nonstring == 'simplerepr':
        try:
            value = str(obj)
        except UnicodeError:
            try:
                value = repr(obj)
            except UnicodeError:
                # Giving up
                return ''
    elif nonstring == 'passthru':
        return obj
    elif nonstring == 'empty':
        return ''
    elif nonstring == 'strict':
        raise TypeError('obj must be a string type')
    else:
        raise TypeError('Invalid value %s for to_text\'s nonstring parameter' % nonstring)

    return to_text(value, encoding, errors)


def uniq(string):
    """Removes duplicate words from a string (only the second duplicates).
    The sequence of the words will not be changed.
    """
    words = string.split()
    return ' '.join(sorted(set(words), key=words.index))


to_native = to_text
