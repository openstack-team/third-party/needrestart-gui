#! /usr/bin/env python3

"""Communicates with the Shell.
"""

import os
import re
import shlex
import subprocess
#from .txt import to_text, to_bytes
from . import txt

def shell_exec(cmd, env=None, shell=False, stdin='', cwd=None, timeout=None):
    """
    Parameters
    ----------
    cmd : str
        Command to spawn the child process.
    env : None or dict
        Environment variables. Example: env={'PATH': '/usr/bin'}.
    shell : bool
        If True, the new process is called via what is set in the SHELL
        environment variable - means using shell=True invokes a program of the
        user's choice and is platform-dependent. It allows you to expand
        environment variables and file globs according to the shell's usual
        mechanism, which can be a security hazard. Generally speaking, avoid
        invocations via the shell. It is very seldom needed to set this
        to True.
    stdin : str
        If set, use this as input into `cmd`.
    cwd : str
        Current Working Directory
    timeout : int

    """
    if not env:
        env = os.environ.copy()
    env['LC_ALL'] = 'C'

    if shell or stdin:
        try:
            p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                                  stderr=subprocess.PIPE, env=env, shell=True, cwd=cwd)
        except OSError as e:
            return (False, 'OS Error "{} {}" calling command "{}"'.format(e.errno, e.strerror, cmd))
        except ValueError as e:
            return (False, 'Value Error "{}" calling command "{}"'.format(e, cmd))
        except e:
            return (False, 'Unknown error "{}" while calling command "{}"'.format(e, cmd))

        if stdin:
            stdout, stderr = p.communicate(input=txt.to_bytes(stdin))
        else:
            stdout, stderr = p.communicate()
        retc = p.returncode
        return (True, (txt.to_text(stdout), txt.to_text(stderr), retc))

    cmds = cmd.split('|')
    p = None
    for cmd in cmds:
        try:
            args = shlex.split(cmd.strip())
            stdin = p.stdout if p else subprocess.PIPE
            p = subprocess.Popen(args, stdin=stdin, stdout=subprocess.PIPE,
                                  stderr=subprocess.PIPE, env=env, shell=False, cwd=cwd)
        except OSError as e:
            return (False, 'OS Error "{} {}" calling command "{}"'.format(e.errno, e.strerror, cmd))
        except ValueError as e:
            return (False, 'Value Error "{}" calling command "{}"'.format(e, cmd))
        except e:
            return (False, 'Unknown error "{}" while calling command "{}"'.format(e, cmd))

    try:
        stdout, stderr = p.communicate(timeout=timeout)
    except subprocess.TimeoutExpired:
        p.kill()
        outs, errs = p.communicate()
        return (False, 'Timeout after {} seconds.'.format(timeout))
    retc = p.returncode
    return (True, (txt.to_text(stdout), txt.to_text(stderr), retc))
