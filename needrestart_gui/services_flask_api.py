from flask import Flask, request, render_template, jsonify, redirect, url_for
from flask_socketio import SocketIO
import os
import datetime
import socket

app = Flask(__name__, template_folder=os.getcwd())
socketio = SocketIO(app)

received_data = {}

@app.route('/update_data', methods=['POST', 'GET'])
def update_data():
    global received_data
    if request.method == 'POST':
        data = request.get_json()
        print("Received data:", data)
        received_data = data
        socketio.emit('data_updated', data, broadcast=True)
        return redirect(url_for('get_data'))
    else:
        return jsonify(received_data)

@app.route('/get_data', methods=['GET'])
def get_data():
    global received_data
    current_time = datetime.datetime.now().strftime('%H:%M')
    server_machine_name = socket.gethostname()
    print(received_data)
    return render_template('needrestart_gui/index_test.html', services=received_data,  machine_name=server_machine_name, refresh_time=current_time)

def main(app=app):
    return app
