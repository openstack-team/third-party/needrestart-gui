from pystemd.systemd1 import Unit, Manager
from datetime import datetime, timedelta
import pytz
from debian import deb822
import json
import requests
from flask import Flask, request, render_template, jsonify
import os, sys
import threading
import schedule
import time
import  needrestart_gui.lib.base
import  needrestart_gui.lib.shell
from  needrestart_gui.lib.globals import (STATE_OK, STATE_UNKNOWN, STATE_WARN)
from oslo_config import cfg
from oslo_log import log as logging

# Codes ANSI pour les couleurs
GREEN = '\033[92m'
RED = '\033[91m'
RESET = '\033[0m'

LOG = logging.getLogger(__name__,project='needrestart-gui')

# Définition du groupe d'options servicesnropts
servicesnropts = cfg.OptGroup(name='servicesnropts', title='Options for my servicesnropts')

servicesnropts_opts = [
    cfg.ListOpt('main_services', default=['alpha','omega'], help='Main services list'),
    cfg.ListOpt('other_services', default=['lorem','ipsum'], help='Other services list'),
    cfg.StrOpt('bind_host', default='0.0.0.0', help='IP address to listen on.')
]

def list_opts():
    return [
        ("servicesnropts", servicesnropts_opts),
    ]

services = [
    "aodh-api", "aodh-evaluator", "aodh-listener", "aodh-notifier",
    "barbican-api", "barbican-worker",
    "ceilometer-agent-notification", "ceilometer-polling",
    "cinder-api", "cinder-scheduler",
    "cloudkitty-api", "cloudkitty-processor",
    "glance-api", "gnocchi-api", "gnocchi-metricd", "gnocchi-statsd",
    "heat-api-cfn", "heat-api", "heat-engine",
    "keystone",
    "magnum-api", "magnum-conductor",
    "neutron-api",
    "neutron-rpc-server",
    "nova-api", "nova-conductor",
    "nova-scheduler",
    "octavia-api", "octavia-health-manager", "octavia-housekeeping", "octavia-worker",
    "placement-api", "designate-api", "designate-central", "designate-mdns",
    "designate-producer", "designate-sink", "designate-worker"
]
def get_package_version(service):
    try:
        # Rechercher le package correspondant au service
        packages = deb822.Deb822.iter_paragraphs(open('/var/lib/dpkg/status'))
        for package in packages:
            if package.get('Package', '').startswith(service):
                version = package.get('Version')
                if version:
                    #print(f"Version for {service}: {version}")
                    return version
        else:
            print(f"No package information found for {service}")
    except Exception as e:
        print(f"An error occurred: {e}")

def check_service(service, service_restart, ip_srv):
    result = {}
    other_service = services
    target_url = f"http://{ip_srv}:5001/update_data"
    for service in services:
        try:
            unit = Unit(service + ".service", _autoload=True)
            active_state = unit.ActiveState.decode('utf-8')
            load_state = unit.LoadState.decode('utf-8')
            status = {
                'service_name': service,
                'active_state': active_state,
                'load_state': load_state,
                'need_restart': 'no'
            }
            if active_state == "active" and load_state == "loaded":
                timestamp = unit.ConditionTimestamp
                timestamp_seconds = timestamp / 1e6
                utc_datetime = datetime.fromtimestamp(timestamp_seconds, tz=pytz.UTC)
                status['start_time'] = utc_datetime.strftime('%Y-%m-%d %H:%M:%S %Z')
                status['package_version'] = get_package_version(service)              
                get_package_version(service)
            else:
                timestamp = unit.ConditionTimestamp
                timestamp_seconds = timestamp / 1e6
                utc_datetime = datetime.fromtimestamp(timestamp_seconds, tz=pytz.UTC)
                status['start_time'] = utc_datetime.strftime('%Y-%m-%d %H:%M:%S %Z')
                status['package_version'] = get_package_version(service)
                get_package_version(service)

            result[service] = status

        except Exception as e:
            print(f"An error occurred while checking '{service}': {e}")
    
    for service, status in result.items():
        service_name = status['service_name']
        service_name_without_suffix = service_name.split('.')[0]
        if service_name_without_suffix in service_restart:
            status['need_restart'] = 'yes'
            print(f"Service {service_name} nécessite un redémarrage.")
    services_json = json.loads(service_restart)
    other = [service for service in services_json['services'] if service.split('.')[0] not in services]
    others = [service for service in other if service]
    obj = {'services': result, 'other_services': others}

    try:
        response = requests.post(target_url, json=obj)
        if response.status_code == 200:
            print("Data sent successfully to the target application.")
        else:
            print("Failed to send data to the target application.")
    except Exception as e:
        print(f"An error occurred while sending data: {e}")

    return json.dumps(result, indent=4)

def service_need_restart():
    """The main function.
    """

    a = True
    if a:
        # 1. check if needrestart is available - if yes, use it
        success, result = lib.shell.shell_exec('needrestart -b')
        if success:
            stdout, stderr, retc = result
            msg = ''
            svc = 0
            svcs = ''
            for line in stdout.split('\n'):
                if line.startswith('NEEDRESTART-KCUR: '):
                    kcur = line.replace('NEEDRESTART-KCUR: ', '')
                if line.startswith('NEEDRESTART-KEXP: '):
                    kexp = line.replace('NEEDRESTART-KEXP: ', '')
                if line.startswith('NEEDRESTART-KSTA: '):
                    if line.endswith('0'):
                        msg += '(Unknown or failed to detect)'
                    if line.endswith('2'):
                        msg += '(ABI compatible upgrade pending)'
                    if line.endswith('3'):
                        msg += '(Version upgrade pending)'
                if line.startswith('NEEDRESTART-SVC: '):
                    svc += 1
                    svcs += '* {}\n'.format(line.replace('NEEDRESTART-SVC: ', ''))
            if kcur != kexp:
                msg = 'Running Kernel {} != Installed Kernel {} {}. '.format(
                    kcur,
                    kexp,
                    msg,
                )
            if svc:
                service_lines = svcs.split('\n')
                cleaned_services = [line.strip('* ') for line in service_lines]
                service_list = {'services': cleaned_services}
                service_json = json.dumps(service_list)
                return service_json

if __name__ == "__main__":
    # Oslo config
    global CONF
    CONF = cfg.ConfigOpts()
    CONF.register_group(servicesnropts)
    CONF.register_cli_opts(servicesnropts_opts, group=servicesnropts)
    logging.register_options(CONF)
    CONF(default_config_files=['config/needrestart-gui.conf'], project='needrestart-gui')
    logging.setup(CONF, CONF.project)
    print(CONF.servicesnropts.bind_host)
    ip_srv = CONF.servicesnropts.bind_host
    #sys.exit()

    # Schedule post data
    snr = service_need_restart()
    json_result = check_service(services, snr, ip_srv)
    schedule.every(1).minutes.do(check_service, service=services, service_restart=snr, ip_srv=ip_srv)
    while True:
        schedule.run_pending()
        time.sleep(1)
